'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
        controller('MyCtrl1', ['$scope', function($scope) {
            alert("MyCtrl1");
            $scope.a = "valdemar";
          }])
        .controller('MyCtrl2', [function() {

          }])
        .controller('HelloCtrl', ['$scope', '$http', function($scope, $http) {
            $scope.a = "valdemar";
            $http.get('http://localhost:8080/RestTest/res/hello').success(function(data) {
              $scope.a = data;
            });
          }]);
